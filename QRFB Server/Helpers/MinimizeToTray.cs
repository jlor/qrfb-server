﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace QRFB_Server.Helpers
{
    internal static class MinimizeToTray
    {
        public static void Enable(Window window)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new MinimizeToTrayInstance(window);
        }
    }

    class MinimizeToTrayInstance
    {
        private readonly Window _window;
        private NotifyIcon _notifyIcon;
        private bool _balloonShown;

        public MinimizeToTrayInstance(Window window)
        {
            _window = window;
            _window.StateChanged += _window_StateChanged;
        }

        void _window_StateChanged(object sender, EventArgs e)
        {
            if (_notifyIcon == null)
            {
                _notifyIcon = new NotifyIcon {Icon = Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location)};

                _notifyIcon.MouseClick += _notifyIcon_MouseClick;
                _notifyIcon.BalloonTipClicked += _notifyIcon_MouseClick;
            }

            _notifyIcon.Text = _window.Title;
            var isMinimized = (_window.WindowState == WindowState.Minimized);
            _window.ShowInTaskbar = !isMinimized;
            _notifyIcon.Visible = isMinimized;
            if (isMinimized && !_balloonShown)
            {
                _notifyIcon.ShowBalloonTip(1000, _window.Title, "Press Alt+Q to serve a window.", ToolTipIcon.None);
                _balloonShown = true;
            }
        }

        private void _notifyIcon_MouseClick(object sender, EventArgs e)
        {
            _window.WindowState = WindowState.Normal;
        }

    }
}
