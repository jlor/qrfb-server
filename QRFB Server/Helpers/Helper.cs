﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QRFB_Server.Helpers
{
    class Helper
    {
        public static IntPtr GetWindowUnderCursor()
        {
            Pinvoke.POINT ptCursor = new Pinvoke.POINT();

            return !(Pinvoke.GetCursorPos(out ptCursor)) ? IntPtr.Zero : Pinvoke.WindowFromPoint(ptCursor);
        }

        public static string GetTextFromIntPtr(IntPtr intPtr)
        {
            int length = Pinvoke.GetWindowTextLength(intPtr);
            StringBuilder stringBuilder = new StringBuilder(length + 1);
            Pinvoke.GetWindowText(intPtr, stringBuilder, stringBuilder.Capacity);
            return stringBuilder.ToString();
        }

        public static string GetNameFromIntPtr(IntPtr intPtr)
        {
            uint lpdwProcessId;
            Pinvoke.GetWindowThreadProcessId(intPtr, out lpdwProcessId);
            IntPtr hProcess = Pinvoke.OpenProcess(0x0410, false, lpdwProcessId);
            StringBuilder stringBuilder = new StringBuilder(1000);
            Pinvoke.GetModuleFileNameEx(hProcess, IntPtr.Zero, stringBuilder, stringBuilder.Capacity);
            Pinvoke.CloseHandle(hProcess);
            return stringBuilder.ToString();
        }

        public static Pinvoke.RECT GetWindowFromIntPtr(IntPtr intPtr)
        {
            Pinvoke.RECT resultRect = new Pinvoke.RECT();
            Pinvoke.GetWindowRect(intPtr, ref resultRect);

            return resultRect;
        }
    }
}
