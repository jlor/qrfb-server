﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QRFB_Server
{
    public partial class OverlayForm : Form
    {

        internal delegate void SetVisibility();

        public OverlayForm(Bitmap bitmap, int x, int y)
        {
            InitializeComponent();

            //TransparencyKey = Color.White;
            BackColor = Color.White;
            FormBorderStyle = FormBorderStyle.None;
            TopMost = true;
            ShowInTaskbar = false;
            Width = bitmap.Width;
            Height = bitmap.Height+10;
            MinimumSize = new Size(Width, Height);
            StartPosition = FormStartPosition.Manual;
            DesktopLocation = new Point(x,y);

            label1.Text = "QRFB code:";
            pictureBox1.Image = bitmap;
            pictureBox1.Height = bitmap.Height;
            pictureBox1.Width = bitmap.Width;

        }

        public override sealed Size MinimumSize
        {
            get { return base.MinimumSize; }
            set { base.MinimumSize = value; }
        }

        public void SetInvisible()
        {
            if (!this.InvokeRequired) return;
            var setVisibility = new SetVisibility(Hide);
            this.Invoke(setVisibility, new object[]{});
        }

        public override sealed Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }
    }
}
