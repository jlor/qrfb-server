﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using QRFB_Server.Helpers;
using ZXing;
using Application = System.Windows.Application;

namespace QRFB_Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region fields
        private ViewModel _viewModel;
        private Hotkey _hotkey;
        private Dictionary<IntPtr, OverlayForm> _overlays;
        private IBarcodeWriter _barcodeWriter;

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            
            _overlays = new Dictionary<IntPtr, OverlayForm>();
            _barcodeWriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE
            };

            DataContext = _viewModel = new ViewModel();

            // We have to wait for the window to fully load before we can assign hotkeys!
            // Because we need to have a handle for the window for the hotkeys to return to :-)
            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            AssignHotkey();
            Helpers.MinimizeToTray.Enable(this);
        }

        void AssignHotkey()
        {
            _hotkey = new Hotkey(Modifiers.Alt, Keys.Q, this, registerImmediately: true);
            _hotkey.HotkeyPressed += _hotkey_HotkeyPressed;
        }

        void _hotkey_HotkeyPressed(object sender, HotkeyEventArgs e)
        {
            IntPtr windowIntPtr = Helper.GetWindowUnderCursor();

            string name = Helper.GetTextFromIntPtr(windowIntPtr);
            Pinvoke.RECT windowRect = Helper.GetWindowFromIntPtr(windowIntPtr);

            int x = Math.Min(windowRect.Left, windowRect.Right);
            int y = Math.Min(windowRect.Top, windowRect.Bottom);
            int width = Math.Abs(windowRect.Left - windowRect.Right);
            int height = Math.Abs(windowRect.Bottom - windowRect.Top);

            // Create a new VNC server for this application
            var model = _viewModel.AddWorker(windowIntPtr, name, x, y, width, height);
            model.PropertyChanged += model_PropertyChanged;

            // Create QR code
            Bitmap barcode = _barcodeWriter.Write("http://" + model.HostName + ":" + (model.Port - 100) + "/vnc_auto.html?host=" + model.HostName + "&port=" + (model.Port - 100));
            // Create Forms window
            OverlayForm overlay = new OverlayForm(barcode, x ,y);
            // Bind created window to IntPtr of application so we can identify later
            if(!_overlays.ContainsKey(windowIntPtr)) _overlays.Add(windowIntPtr, overlay);
            // Show window!
            overlay.Show();
            
        }

        void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Did we get a "ClientConnected" event?
            if (!e.PropertyName.Equals("ClientConnected")) return;

            // Do we have a model?
            var model = sender as Model;
            if (model == null) return;

            // Do we have the models IntPtr in our map?
            IntPtr intPtr = model.WindowIntPtr;
            OverlayForm overlay;
            if (!_overlays.TryGetValue(intPtr, out overlay)) return;

            // Ok, great! Kill the overlay and remove it from the map!
            overlay.SetInvisible();
            _overlays.Remove(intPtr);
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            foreach (var overlay in _overlays.Values)
                if (overlay != null) overlay.Close();
            
            _hotkey.Unregister();
            _viewModel.StopAll();
            Application.Current.Shutdown();
        }
    }
}
