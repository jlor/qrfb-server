﻿using System;
using System.Windows.Input;

namespace QRFB_Server
{
    public class DelegateCommand : ICommand
    {
        public delegate void SimpleEventHandler();
        private readonly SimpleEventHandler _simpleEventHandler;
        private bool _isEnabled = true;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(SimpleEventHandler handler)
        {
            _simpleEventHandler = handler;
        }

        private void OnCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }

        bool ICommand.CanExecute(object arg)
        {
            return IsEnabled;
        }

        void ICommand.Execute(object arg)
        {
            _simpleEventHandler();
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }

            set
            {
                _isEnabled = value;
                OnCanExecuteChanged();
            }
        }
    }  
}
