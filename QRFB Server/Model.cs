﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using NVNC;

namespace QRFB_Server
{
    class Model : INotifyPropertyChanged
    {
        #region fields

        public string Resolution
        {
            get { return String.Format("{0}x{1}", Width, Height); }
        }
        public string Name
        {
            get { return _name; }
            private set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public int Port
        {
            get { return _port; }
            private set
            {
                _port = value;
                NotifyPropertyChanged("Port");
            }
        }
        public int X { 
            get { return _x; }
            private set
            {
                _x = value;
                NotifyPropertyChanged("X");
            }
        }
        public int Y
        {
            get { return _y; }
            private set
            {
                _y = value;
                NotifyPropertyChanged("Y");
            }
        }
        public int Width
        {
            get { return _width; }
            private set
            {
                _width = value;
                NotifyPropertyChanged("Width");
                NotifyPropertyChanged("Resolution");
            }
        }
        public int Height
        {
            get { return _height; }
            private set
            {
                _height = value;
                NotifyPropertyChanged("Height");
                NotifyPropertyChanged("Resolution");
            }
        }

        public string HostName { get; private set; }

        public IntPtr WindowIntPtr
        {
            get { return _windowIntPtr; }
            private set { _windowIntPtr = value; }
        }
        private readonly VncServer _vncServer;
        private string _name;
        private int _port;
        private int _x;
        private int _y;
        private int _width;
        private int _height;
        private IntPtr _windowIntPtr;
        protected bool ClientConnected = false;
        private readonly Process _websockifyProcess;
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region constructor
        public Model(IntPtr windowIntPtr, string name, int port, int x, int y, int width, int height)
        {
            Name = string.IsNullOrEmpty(name) ? "[Empty name]" : name;
            Port = port;
            X = x;
            Y = y;
            Width = width;
            Height = height;
            HostName = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                    .First(address => address.AddressFamily == AddressFamily.InterNetwork)
                    .ToString();
            WindowIntPtr = windowIntPtr;

            // Let's make this server run in a thread!
            _vncServer = new VncServer("", Port, Name, Width, Height, X, Y);
            _vncServer.ClientConnected += _vncServer_ClientConnected;

            // Start a websockify server with novnc etc.
            var basePath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory;
            var websockifyPath = basePath + "\\websockify\\";
            var websockify = @"" + websockifyPath + "node.exe";
            var processStartInfo = new ProcessStartInfo
            {
                FileName = Path.GetFullPath(websockify),
                // ReSharper disable once AssignNullToNotNullAttribute
                WorkingDirectory = Path.GetDirectoryName(websockify),
                UseShellExecute = false,
                CreateNoWindow = true,
                Arguments = "websockify.js --web ../novnc/ " +
                            HostName + ":" + (port - 100) + " " +
                            HostName + ":" + port
            };
            _websockifyProcess = Process.Start(processStartInfo);


            Thread vncServerThread = new Thread(_vncServer.Start);
            vncServerThread.Start();
        }
        #endregion

        void _vncServer_ClientConnected(object sender, EventArgs e)
        {
            ClientConnected = true;
            NotifyPropertyChanged("ClientConnected");
        }

        internal void Stop()
        {
            try
            {
                _websockifyProcess.Kill();
                _vncServer.Stop();
            }
            catch (Exception e)
            {
                Debug.Print("Exception: {0}", e.InnerException);
                // Aww, I get a null reference because the NVNC library is poorly coded :(
            }
        }

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
