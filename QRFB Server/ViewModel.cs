﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace QRFB_Server
{
    class ViewModel
    {
        private const int DefaultPort = 5900;
        private const int MaxWorkers = 32;

        public ObservableCollection<Model> Workers { get; set; }
        private Model _selectedModel;
        
        public ViewModel()
        {
            Workers = new ObservableCollection<Model>();
        }

        internal Model AddWorker(IntPtr windowIntPtr, string name, int x, int y, int width, int height)
        {
            var currentPorts = Workers.Select(model => model.Port).ToList();
            // Let's find the first available port number from defaultport -> defaultport+maxworkers
            var port =
                Enumerable.Range(DefaultPort, DefaultPort + MaxWorkers)
                    .Except(currentPorts)
                    .DefaultIfEmpty(-1)
                    .FirstOrDefault();
            // No space for another worker!
            if (port < 0) throw new Exception("Max workers reached.");

            var worker = new Model(windowIntPtr, name, port, x, y, width, height);
            Workers.Add(worker);
            return worker;
        }

        internal void StopAll()
        {
            foreach (var model in Workers)
            {
                model.Stop();
            }
        }

        public Model SelectedModel
        {
            get { return _selectedModel; }
            set
            {
                if (value != _selectedModel)
                {
                    _selectedModel = value;
                    NotifyPropertyChanged("SelectedModel");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #region commands
        public ICommand StopServer
        {
            get { return new DelegateCommand(OnStop); }
        }

        private void OnStop()
        {
            if (_selectedModel == null) return;
            _selectedModel.Stop();
            Workers.Remove(_selectedModel);
        }
        #endregion

    }
}
