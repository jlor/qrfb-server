This is the repository of QRFB Server, made by Jakob Rosenlund as proof of concept during his masters thesis at Computer Science at Aarhus University.

This project uses libraries from:

* NVNC (http://nvnc.codeplex.com)

* ZXing (http://zxing.github.io/zxing/)

* NoVNC (https://github.com/kanaka/noVNC)

* The WebSockify nodeJS script (https://github.com/kanaka/websockify/tree/master/other)

If you have any questions, you can reach me at my email: jlor-at-cs.au.dk.